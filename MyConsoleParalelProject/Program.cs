﻿using System.Diagnostics;

namespace MyConsoleParalelProject
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Hello, please enter length for random int array: ");
                int len;
                if (!int.TryParse(Console.ReadLine(), out len))
                {
                    Console.WriteLine("Enter not correct!");
                    return;
                }
                Proc(len);
                Console.WriteLine();
            }
        }

        private static void Proc(int len)
        {
            int[] array = GenerateArray(len);
            Sum sum = new Sum(array);
            var stopWatch = new Stopwatch();

            Action("without thread", stopWatch, SumWithoutThread, sum);
            Action("with thread", stopWatch, SumWithThread, sum);
            Action("with PLINQ", stopWatch, SumWithPLINQ, sum);
        }

        static long SumWithoutThread(Sum sum)
        {
            return sum.GetSumWithoutThread();
        }

        static long SumWithThread(Sum sum)
        {
            return sum.GetSumWithThread();
        }

        static long SumWithPLINQ(Sum sum)
        {
            return sum.GetSumWithPLINQ();
        }

        static void Action(string name, Stopwatch stopWatch, Func<Sum,long> funkSum, Sum sum)
        {
            Console.WriteLine($"Start sum {name}...");
            stopWatch.Start();
            var result = funkSum(sum);
            stopWatch.Stop();
            Console.WriteLine($"Sum {name} finish after {stopWatch.Elapsed} second. Result: {result}");
            Console.WriteLine();
        }

        static int[] GenerateArray(int len)
        {
            int[] array = new int[len];
            Random r = new Random();
            for (int i = 0; i < len; i++)
            {
                array[i] = r.Next(10);
            }
            return array;
        }
    }
}