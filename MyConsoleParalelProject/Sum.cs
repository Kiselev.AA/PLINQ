﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyConsoleParalelProject
{
    public class Sum
    {
        private int[] array;

        public Sum(int[] array)
        {
            this.array = array;
        }

        public long GetSumWithoutThread()
        {
            long sum = 0;
            foreach (var item in array)
                sum+=item;
            return sum;
        }

        public long GetSumWithThread()
        {
            long sum = 0;
            int numberThreads = 10;
            var threads = new List<Thread>();
            var sumArr=new List<long> ();
            var e = new CountdownEvent(numberThreads);
            for (int i = 0; i < numberThreads; i++)
            {
                sumArr.Add(0);
                var numb = i;
                threads.Add(new Thread(() => { SumInThread(numb, numberThreads, e, sumArr); }));
                threads[i].Start();
            }
            e.Wait();
            foreach (var item in sumArr)
                sum += item;

            return sum;
        }

        private void SumInThread(int numberThisThread, int numberThreads, CountdownEvent e,  List<long> result)
        {
            for (int i = 0 + numberThisThread; i < array.Length; i += numberThreads)
            {
                result[numberThisThread] += array[i];
            }
            e.Signal();
        }

        public long GetSumWithPLINQ()
        {
            long sum = array.AsParallel().Sum();
            return sum;
        }

    }
}
